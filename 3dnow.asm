; SPDX-License-Identifier: MIT
; Copyright (c) 2004 Michael Ortmann

; haar wavelet decomposition and short to float in x86 3dnow assembly

; the asm_short_to_float is not only 3dnow, its dsp, so its athlon only 3dnow
; code. its used to convert 16bit integers from a wav file into a floating point
; array. its the idea of for (i = ..., i < ..., i++) b[i] = (float) a[i] with
; float b[] and int a[]

section .text

global asm_dhaar
global asm_short_to_float

; for (i = 0; i < l; i++) {
;   a = transSignal[j++];
;   b = transSignal[j++];
;   transSignal[i] = (a + b) * PRE_SQRT2_D_2;
;   d[i] = (b - a) * PRE_SQRT2_D_2;
; }   

asm_dhaar:
        push    ebp
        mov     ebp, esp
        push    edi
        push    esi
        push    ebx
        femms

        mov     eax, [ebp+20]   ; lade die konstante
        movq    mm0, [eax]

        mov     esi, [ebp+8]    ; esi -> transSignal[j]
        mov     edi, esi        ; edi -> transSignal[i]
        mov     eax, [ebp+12]   ; eax -> d[i]
        mov     ecx, [ebp+16]   ; ecx = l
        shr     ecx, 1          ; ecx = l / 2
loop0:
        movd            mm1, [esi]      ; 0 | a0
        punpckldq       mm1, [esi + 8]  ; a1 | a0
        movd            mm2, [esi + 4]  ; 0 | b0
        punpckldq       mm2, [esi + 12] ; b1 | b0

        movq    mm3, mm1        ; aa

        pfadd   mm1, mm2        ; a + b
        pfsub   mm2, mm3        ; b - a

        pfmul   mm1, mm0        ; * const
        pfmul   mm2, mm0        ; * const
        movq    [edi], mm1
        movq    [eax], mm2

        add     esi, 16
        add     edi, 8
        add     eax, 8

        loop    loop0

        femms
        pop     ebx
        pop     esi
        pop     edi
        leave
        ret

asm_short_to_float:
        push    ebp
        mov     ebp, esp
        push    edi
        push    esi
        push    ebx
        femms

        mov     esi, [ebp+8]    ; src
        mov     edi, [ebp+12]   ; dst
        mov     ecx, [ebp+16]   ; len

        mov     eax, 0          ; eax = len % 2
        shr     ecx, 1          ; len = len / 2
        jnc     loop1
        mov     eax, 1          ; eax = len % 2
loop1:
        movd            mm0, [esi]      ; 0 0 b a
        punpcklwd       mm0, mm0        ; b b a a
        pi2fw           mm0, mm0        ; xb = float(b) | xa = float(a)
        movq            [edi], mm0      ; store xb | xa

        add     esi, 4
        add     edi, 8

        loop    loop1

        cmp     eax, 0          ; if (len % 2 == 1) copy the one left (no conversion here :()
        jne     fini
        mov     eax, [esi]
        mov     [edi], eax

fini:
        femms
        pop     ebx
        pop     esi
        pop     edi
        leave
        ret
